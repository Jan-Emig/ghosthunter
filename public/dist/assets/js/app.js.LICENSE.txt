/*
object-assign
(c) Sindre Sorhus
@license MIT
*/

/*! ../../data/ghost-evidences.json */

/*! ../../data/ghost-names.json */

/*! ../../data/ghost-types.json */

/*! ../../data/objectives.json */

/*! ../../helpers/genderType */

/*! ../../helpers/ghostFilter */

/*! ../helpers/genderType */

/*! ./cjs/react-dom.development.js */

/*! ./cjs/react.development.js */

/*! ./cjs/scheduler-tracing.development.js */

/*! ./cjs/scheduler.development.js */

/*! ./components/evidence */

/*! ./components/footer */

/*! ./components/ghost */

/*! ./components/header */

/*! ./components/missionInfo */

/*! object-assign */

/*! react */

/*! react-dom */

/*! scheduler */

/*! scheduler/tracing */

/*!************************!*\
  !*** ./src/js/app.tsx ***!
  \************************/

/*!**********************************!*\
  !*** ./src/data/objectives.json ***!
  \**********************************/

/*!***********************************!*\
  !*** ./src/data/ghost-names.json ***!
  \***********************************/

/*!***********************************!*\
  !*** ./src/data/ghost-types.json ***!
  \***********************************/

/*!***********************************!*\
  !*** ./src/helpers/genderType.ts ***!
  \***********************************/

/*!*************************************!*\
  !*** ./node_modules/react/index.js ***!
  \*************************************/

/*!*************************************!*\
  !*** ./src/helpers/ghostFilter.tsx ***!
  \*************************************/

/*!*************************************!*\
  !*** ./src/js/components/ghost.tsx ***!
  \*************************************/

/*!**************************************!*\
  !*** ./src/js/components/footer.tsx ***!
  \**************************************/

/*!**************************************!*\
  !*** ./src/js/components/header.tsx ***!
  \**************************************/

/*!***************************************!*\
  !*** ./src/data/ghost-evidences.json ***!
  \***************************************/

/*!****************************************!*\
  !*** ./src/js/components/evidence.tsx ***!
  \****************************************/

/*!*****************************************!*\
  !*** ./node_modules/react-dom/index.js ***!
  \*****************************************/

/*!*****************************************!*\
  !*** ./node_modules/scheduler/index.js ***!
  \*****************************************/

/*!*******************************************!*\
  !*** ./node_modules/scheduler/tracing.js ***!
  \*******************************************/

/*!*******************************************!*\
  !*** ./src/js/components/missionInfo.tsx ***!
  \*******************************************/

/*!*********************************************!*\
  !*** ./node_modules/object-assign/index.js ***!
  \*********************************************/

/*!*****************************************************!*\
  !*** ./node_modules/react/cjs/react.development.js ***!
  \*****************************************************/

/*!*************************************************************!*\
  !*** ./node_modules/react-dom/cjs/react-dom.development.js ***!
  \*************************************************************/

/*!*************************************************************!*\
  !*** ./node_modules/scheduler/cjs/scheduler.development.js ***!
  \*************************************************************/

/*!*********************************************************************!*\
  !*** ./node_modules/scheduler/cjs/scheduler-tracing.development.js ***!
  \*********************************************************************/

/**
 * Checks if an event is supported in the current execution environment.
 *
 * NOTE: This will not work correctly for non-generic events such as `change`,
 * `reset`, `load`, `error`, and `select`.
 *
 * Borrows from Modernizr.
 *
 * @param {string} eventNameSuffix Event name, e.g. "click".
 * @return {boolean} True if the event is supported.
 * @internal
 * @license Modernizr 3.0.0pre (Custom Build) | MIT
 */
